﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AltiumTest.Model;
using AltiumTest.Model.Preferences;
using AltiumTest.Model.Quote;

namespace AltiumTest.Exstensions
{
	public static class CommonExtensions
	{

		public static void SetDefaultPreferences(this IEnumerable<PreferencesGroup> preferencesGroups)
		{
			foreach (var gr in preferencesGroups)
			{
				gr.SetDefaultPreferences();
			}
		}

		public static void AddBoardsQuantity(this ObservableCollection<QuoteParameter> quoteParams, IEnumerable<PreferencesGroup> preferencesGroups)
		{
			var boardQuantityPreferance = preferencesGroups.SelectMany(f => f.Preferences).
															SingleOrDefault(s => s.GetType() == typeof(PreferenceBoardsQuantity)) as PreferenceBoardsQuantity;
			if (boardQuantityPreferance != null)
			{
				quoteParams.Add(new QuoteParameterFabrication("Boards Quantity", boardQuantityPreferance.Quantity.ToString(),
															  boardQuantityPreferance.AdditionalTime,
															  boardQuantityPreferance.AdditionalCost));
			}
		}

		public static void AddSurfaceFinish(this ObservableCollection<QuoteParameter> quoteParams, IEnumerable<PreferencesGroup> preferencesGroups)
		{
			var surfaceFinishPreferance = preferencesGroups.SelectMany(f => f.Preferences).
															SingleOrDefault(s => s.GetType() == typeof(PreferenceSurfaceFinish)) as PreferenceSurfaceFinish;

			if (surfaceFinishPreferance != null && surfaceFinishPreferance.SelectedPayload != null)
			{
				quoteParams.Add(new QuoteParameterFabrication("Surface Finish",
															  surfaceFinishPreferance.SelectedPayload.Value,
															  surfaceFinishPreferance.AdditionalTime, (decimal)surfaceFinishPreferance.AdditionalCost));
			}

		}

		public static void AddAdditional(this ObservableCollection<QuoteParameter> quoteParams, IEnumerable<PreferencesGroup> preferencesGroups)
		{
			quoteParams.Add(new QuoteParameterFabrication("Base fabrication", "61.72x148.84mm, 10layers", 1, 1097));

			quoteParams.Add(new QuoteParameterAssembly("Base fabrication", "Package on packages", 1, 2697));
			quoteParams.Add(new QuoteParameterAssembly("Processes", "Split Assembly", 0, 720.5m));
			quoteParams.Add(new QuoteParameterAssembly("Minimum Pitch", "0.3mm pitch BGA", 0, 804));

			quoteParams.Add(new QuoteParameterComponents("Microchip ATTINY2313-20SU", "2", 0, 48.64m));
			for (int i = 0; i < 10; i++)
			{
				quoteParams.Add(new QuoteParameterComponents("Microchip ATTINY2313-20PC", "2", 0, 22.12m));
			}
			quoteParams.Add(new QuoteParameterComponents("Microchip ATTINY2313-20SU", "2", 0, 48.64m));
		}
	}
}
