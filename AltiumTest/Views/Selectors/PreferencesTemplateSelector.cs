﻿using System;
using System.Windows;
using System.Windows.Controls;
using AltiumTest.Model.Preferences;

namespace AltiumTest.Views.Selectors
{
	public class PreferencesTemplateSelector : DataTemplateSelector
	{
		public DataTemplate PreferenceProjectNameTemplate { get; set; }
		public DataTemplate PreferenceBoardsQuantityTemplate { get; set; }
		public DataTemplate PreferenceValidatableTemplate { get; set; }
		public DataTemplate PreferenceMaterialSurfaceFinishTemplate { get; set; }

		public DataTemplate PreferenceColorComboBoxTemplate { get; set; }
		public DataTemplate PreferenceBinarySelectionTemplate { get; set; }
		public DataTemplate PreferenceCooperWeightTemplate { get; set; }
		public DataTemplate PreferenceBinarySelectionTemplateLong { get; set; }
		public DataTemplate PreferenceNotesTemplate { get; set; }
		public override DataTemplate SelectTemplate(object item,
			DependencyObject container)
		{

			if (item is PreferenceProjectName)
				return PreferenceProjectNameTemplate;
			else if (item is PreferenceBoardThickness)
				return PreferenceBoardsQuantityTemplate;
			else if (item is PreferenceBoardsQuantity)
				return PreferenceValidatableTemplate;
			else if (item is PreferenceZipCode)
				return PreferenceValidatableTemplate;
			else if (item is PreferenceMaterial || item is PreferenceSurfaceFinish)
				return PreferenceMaterialSurfaceFinishTemplate;
			else if (item is PreferenceSolderMask || item is PreferenceSilkScreen)
				return PreferenceColorComboBoxTemplate;
			else if (item is PreferenceTentingForVias)
				return PreferenceBinarySelectionTemplateLong;
			else if (item is PreferenceBinarySelection)
				return PreferenceBinarySelectionTemplate;
			else if (item is PreferenceCooperInner || item is PreferenceCooperOuter)
				return PreferenceCooperWeightTemplate;
			else if (item is PreferenceNotes)
				return PreferenceNotesTemplate;
			else
				throw new Exception($"Can't find template in {nameof(PreferencesTemplateSelector)}");
		}
    }
}