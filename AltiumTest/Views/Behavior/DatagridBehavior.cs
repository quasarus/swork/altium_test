﻿using Microsoft.Xaml.Behaviors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace AltiumTest.Views.Behavior
{
	public class DatagridBehavior : Behavior<DataGrid>
	{
		protected override void OnAttached()
		{
			AssociatedObject.Unloaded += AssociatedObject_Unloaded;
			AssociatedObject.SelectedCellsChanged += AssociatedObject_SelectedCellsChanged;
			base.OnAttached();
		}

		private void AssociatedObject_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
		{
			AssociatedObject.SelectedCellsChanged -= AssociatedObject_SelectedCellsChanged;
			AssociatedObject.UnselectAll();
		}

		protected override void OnDetaching()
		{
			AssociatedObject.Unloaded -= AssociatedObject_Unloaded;
			AssociatedObject.SelectedCellsChanged -= AssociatedObject_SelectedCellsChanged;
			base.OnDetaching();
		}

		private void AssociatedObject_Unloaded(object sender, System.Windows.RoutedEventArgs e)
		{
			AssociatedObject.Unloaded -= AssociatedObject_Unloaded;
			AssociatedObject.SelectedCellsChanged -= AssociatedObject_SelectedCellsChanged;
		}
	}
}
