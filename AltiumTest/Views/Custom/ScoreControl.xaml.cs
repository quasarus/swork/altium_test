﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AltiumTest.Views.Custom
{
	/// <summary>
	/// Interaction logic for ScoreControl.xaml
	/// </summary>
	public partial class ScoreControl : UserControl
	{


		public int Score
		{
			get { return (int)GetValue(ScoreProperty); }
			set { SetValue(ScoreProperty, value); }
		}

		// Using a DependencyProperty as the backing store for Score.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ScoreProperty =
		DependencyProperty.Register("Score", typeof(int), typeof(ScoreControl), new PropertyMetadata(0));



		public SolidColorBrush DisabledColor
		{
			get { return (SolidColorBrush)GetValue(DisabledColorProperty); }
			set { SetValue(DisabledColorProperty, value); }
		}

		// Using a DependencyProperty as the backing store for DisabledColor.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty DisabledColorProperty =
			DependencyProperty.Register("DisabledColor", typeof(SolidColorBrush), typeof(ScoreControl), new PropertyMetadata(null));



		public SolidColorBrush EnabledColor
		{
			get { return (SolidColorBrush)GetValue(EnabledColorProperty); }
			set { SetValue(EnabledColorProperty, value); }
		}

		// Using a DependencyProperty as the backing store for EnabledColor.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty EnabledColorProperty =
			DependencyProperty.Register("EnabledColor", typeof(SolidColorBrush), typeof(ScoreControl), new PropertyMetadata(null));




		public ScoreControl()
		{
			InitializeComponent();
		}
	}

	public class ScoreEllipseBackgroundConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			//#f0c425
			//#e9bd1a- 
			int score = (int)values[0];
			int position = (int)values[1];
			object disabledColor = values[2];
			object enabledColor = values[3];

			return score < position ? disabledColor : enabledColor;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
