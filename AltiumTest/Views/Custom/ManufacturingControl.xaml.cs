﻿namespace AltiumTest.Views.Custom
{
	using AltiumTest.Model.Manufacturing;
	using System;
	using System.Collections;
	using System.Collections.ObjectModel;
	using System.Globalization;
	using System.Linq;
	using System.Windows;
	using System.Windows.Controls;
	using System.Windows.Data;
	/// <summary>
	/// Interaction logic for ManufacturingControl.xaml
	/// </summary>
	public partial class ManufacturingControl : UserControl
	{
		public IEnumerable Source
		{
			get { return (IEnumerable)GetValue(SourceProperty); }
			set { SetValue(SourceProperty, value); }
		}

		// Using a DependencyProperty as the backing store for Source.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty SourceProperty =
			DependencyProperty.Register("Source", typeof(IEnumerable), typeof(ManufacturingControl), new PropertyMetadata(null));


		public double StripHeight
		{
			get { return (double)GetValue(StripHeightProperty); }
			set { SetValue(StripHeightProperty, value); }
		}

		// Using a DependencyProperty as the backing store for Source.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty StripHeightProperty =
			DependencyProperty.Register("StripHeight", typeof(double), typeof(ManufacturingControl), new PropertyMetadata((double)20));


		public ManufacturingControl()
		{
			InitializeComponent();
			layout.SizeChanged += Layout_SizeChanged;
		}

		private void Layout_SizeChanged(object sender, SizeChangedEventArgs e)
		{

			var manufGroup = manufDesrItemsControl.ItemsSource as ObservableCollection<ManufacturingGroup>;
			if (manufGroup == null)
				return;


			//for (int cnt = 0; cnt < manufDesrItemsControl.Items.Count - 1; cnt++)
			//{
			//	var stackPanel = GetStackPanel(cnt);

			//	if (stackPanel == null)
			//		continue;

			//	var totalCost = manufGroup.Sum(r => r.Cost);
			//	ManufacturingGroup gr = manufGroup.ElementAt(cnt);

			//	var stripWidth = (double)gr.Cost / (double)totalCost * e.NewSize.Width;

			//	if (cnt == manufDesrItemsControl.Items.Count - 2)
			//	{
			//		var lastStackPanel = GetStackPanel(cnt + 1);
			//		ManufacturingGroup lasrGr = manufGroup.ElementAt(cnt + 1);
			//		var lastStripWidth = (double)lasrGr.Cost / (double)totalCost * e.NewSize.Width;
			//		var width = stripWidth + lastStripWidth - lastStackPanel.ActualWidth;
			//		if (width > 0)
			//		{
			//			stackPanel.Width = width;
			//		}


			//	}
			//	else
			//	{
			//		var nextStackPanel = GetStackPanel(cnt + 1);
			//		ManufacturingGroup nextrGr = manufGroup.ElementAt(cnt + 1);
			//		var nextStripWidth = (double)nextrGr.Cost / (double)totalCost * e.NewSize.Width;


			//		if (nextStripWidth > stackPanel.ActualWidth + nextStackPanel.ActualWidth)
			//		{
			//			if (stackPanel.ActualWidth <= stripWidth)
			//			{
			//				stackPanel.Width = stripWidth;
			//			}
			//		}
			//		else
			//		{
			//			stackPanel.Width = stripWidth - nextStackPanel.ActualWidth;
			//		}


			//	}

			//}


		}

		private StackPanel GetStackPanel(int cnt)
		{
			ContentPresenter contentPresenter =
				(ContentPresenter)manufDesrItemsControl.ItemContainerGenerator.ContainerFromItem(manufDesrItemsControl.Items[cnt]);
			StackPanel stackPanel = contentPresenter.ContentTemplate.FindName("manufDesrStackPanel", contentPresenter) as StackPanel;
			return stackPanel;
		}
	}

	public class ManufacturingControlWidthConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			if (values.Any(f => f == DependencyProperty.UnsetValue))
				return 0;

			var manufacturers = (ObservableCollection<ManufacturingGroup>)values[0];
			var pair = (ManufacturingGroup)values[1];
			var totalWidth = (double)values[2];

			var totalCost = manufacturers.Sum(r => r.Cost);

			return (double)pair.Cost / (double)totalCost * totalWidth;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}

	public class ManufacturingGroupCostDescriptionConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			if (values.Any(f => f == DependencyProperty.UnsetValue))
				return 0;

			var manufacturers = (ObservableCollection<ManufacturingGroup>)values[0];
			var pair = (ManufacturingGroup)values[1];
			var totalWidth = (double)values[2];

			var totalCost = manufacturers.Sum(r => r.Cost);
			double percent = (double)(100 * pair.Cost) / (double)totalCost;


			return $"{pair.Name} - {percent.ToString(("0.0"))}%";
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}

	public class ManufacturingGroupCostConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var pair = (ManufacturingGroup)value;

			return $"${pair.Cost.ToString("0.00")}";
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}

	public class ManufacturingGroupDescriptionPositionConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			if (values.Any(f => f == DependencyProperty.UnsetValue))
				return (double)0;

			var totalControlWidth = (double)values[2];
			if (totalControlWidth == 0)
				return (double)0;

			var descrActualWidth = (double)values[4];
			if (descrActualWidth == 0)
				return (double)0;

			var index = (int)values[3];

			var manufacturers = (ObservableCollection<ManufacturingGroup>)values[0];

			var pair = (ManufacturingGroup)values[1];

			var totalCost = manufacturers.Sum(r => r.Cost);

			var stripWidth = (double)pair.Cost / (double)totalCost * totalControlWidth;


			double[] allDescrWidths = new double[manufacturers.Count];


			for (int cnt = 0; cnt < manufacturers.Count; cnt++)
			{
				double currentWidth = 0;
				//first element
				if (index == 0)
				{
					currentWidth = descrActualWidth <= stripWidth ? stripWidth : descrActualWidth;
				}
				else
				{
					if (cnt == manufacturers.Count - 2)
					{
						var lastPair = manufacturers.Last();
						var lastStripWidth = (double)pair.Cost / (double)totalCost * totalControlWidth;

					}
					else
					{
						if (descrActualWidth <= stripWidth)
						{
							currentWidth = stripWidth;
						}
						else
						{
							currentWidth = descrActualWidth;
						}
					}


				}

				if (cnt == index)
				{
					return currentWidth;
				}
			}

			return (double)0;

		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
