﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Prism.Commands;

namespace AltiumTest.Views.Custom
{
	/// <summary>
	/// Interaction logic for BinarySelectorUserControl.xaml
	/// </summary>
	public partial class BinarySelectorUserControl : UserControl
	{

		public IEnumerable Source
		{
			get { return (IEnumerable)GetValue(SourceProperty); }
			set { SetValue(SourceProperty, value); }
		}

		// Using a DependencyProperty as the backing store for Source.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty SourceProperty =
			DependencyProperty.Register("Source", typeof(IEnumerable), typeof(BinarySelectorUserControl), new PropertyMetadata(null));



		public object SelectedValue
		{
			get { return (object)GetValue(SelectedValueProperty); }
			set { SetValue(SelectedValueProperty, value); }
		}

		// Using a DependencyProperty as the backing store for SelectedValue.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty SelectedValueProperty =
			DependencyProperty.Register("SelectedValue", typeof(object), typeof(BinarySelectorUserControl), new PropertyMetadata(null));





		public DelegateCommand<object> ButtonSelectedCommand { get; set; }

		public BinarySelectorUserControl()
		{
			InitializeComponent();
			ButtonSelectedCommand = new DelegateCommand<object>(OnButtonSelectedCommand);
		}

		private void OnButtonSelectedCommand(object buttonDataContext)
		{
			if (buttonDataContext == null)
				return;

			SelectedValue = buttonDataContext;

			foreach (var srcItem in Source)
			{
				if (srcItem != buttonDataContext)
				{
					PropertyInfo prop = srcItem.GetType().GetProperty("IsSelected", BindingFlags.Public | BindingFlags.Instance);
					if (prop != null && prop.CanWrite)
					{
						prop.SetValue(srcItem, false);
					}
				}
			}
		}
	}

	public class AlternationIndexToBooleanConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			if (values[0] == DependencyProperty.UnsetValue || values[1] == DependencyProperty.UnsetValue)
				return null;

			int alternationIndex = (int)values[0];
			int count = (int)values[1];
			if (count == 0)
				return null;

			//first index
			if (alternationIndex == 0)
			{
				return false;
			}

			//last index
			if (alternationIndex == (count - 1))
			{
				return true;
			}

			return null;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			return new object[] { };
		}
	}

	public class BinarySelectorWidthConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			if (values[0] == DependencyProperty.UnsetValue || values[1] == DependencyProperty.UnsetValue)
				return (double)0;


			double parentWidth = (double) values[0];
			int buttonsNum = (int)values[1];

			return parentWidth / buttonsNum;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
