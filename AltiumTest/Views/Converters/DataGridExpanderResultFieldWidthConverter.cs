﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace AltiumTest.Views.Converters
{
    public class DataGridExpanderResultFieldWidthConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Any(f => f == DependencyProperty.UnsetValue))
                return (double)0;


            double parameterColoumnWidth = (double)values[0];
            double valueColoumnWidth = (double)values[1];
            double headerToggleButtonWidth = (double)values[2];
            return parameterColoumnWidth + valueColoumnWidth - headerToggleButtonWidth;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
