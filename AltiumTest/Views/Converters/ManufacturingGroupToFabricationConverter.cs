﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using AltiumTest.Model.Manufacturing;

namespace AltiumTest.Views.Converters
{
	public class ManufacturingGroupToFabricationConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var manufGroups = value as IEnumerable<ManufacturingGroup>;
			if (manufGroups == null)
				return null;

			var fabricationGroup = manufGroups.SingleOrDefault(f => f.Name == "Fabrication");
			if (fabricationGroup == null)
				return null;

			return $"${fabricationGroup.Cost.ToString("0.00")}, {fabricationGroup.DayImpact} days";
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
