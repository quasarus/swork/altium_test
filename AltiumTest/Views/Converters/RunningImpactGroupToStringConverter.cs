﻿using AltiumTest.Model.Quote;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.Eventing.Reader;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace AltiumTest.Views.Converters
{
    public class RunningImpactGroupToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            ReadOnlyObservableCollection<object> parameters = value as ReadOnlyObservableCollection<object>;
            if (parameters == null) return null;
            var param = (string)parameter;
            if (param == "Days")
            {
                return $"{parameters.Cast<QuoteParameter>().Sum((f => f.ImpactDaysValue))} days";
            }

            if (param == "Cost")
            {
                return $"${parameters.Cast<QuoteParameter>().Sum((f => f.CostImpactValue)).ToString("0.00")}";
            }
            return null;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
