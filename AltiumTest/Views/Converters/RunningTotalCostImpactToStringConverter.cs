﻿using AltiumTest.Model.Quote;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace AltiumTest.Views.Converters
{
	public class RunningTotalCostImpactToStringConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			IEnumerable<QuoteParameter> quoteParameters = value as IEnumerable<QuoteParameter>;
			if (quoteParameters == null)
				return null;

			return $"${quoteParameters.Sum(f => f.CostImpactValue)}";
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
