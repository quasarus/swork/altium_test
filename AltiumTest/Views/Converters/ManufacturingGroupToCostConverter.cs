﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using AltiumTest.Model.Manufacturing;

namespace AltiumTest.Views.Converters
{
	public class ManufacturingGroupToCostConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var manufGroups = value as IEnumerable<ManufacturingGroup>;
			if (manufGroups == null)
				return null;

			return $"${manufGroups.Sum(f=>f.Cost).ToString("0.00")}";
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
