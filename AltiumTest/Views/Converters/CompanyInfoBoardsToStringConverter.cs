﻿using AltiumTest.Model.Quote;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using AltiumTest.Model;
using AltiumTest.Model.Preferences;

namespace AltiumTest.Views.Converters
{
	public class CompanyInfoBoardsToStringConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var quoteParameters = (IEnumerable<PreferencesGroup>) value;
			var boardQuantityPref = quoteParameters.SelectMany(v => v.Preferences)
				.SingleOrDefault(f => f.GetType() == typeof(PreferenceBoardsQuantity)) as PreferenceBoardsQuantity;

			if (boardQuantityPref == null)
				return null;

			uint boardQuantity = boardQuantityPref.Quantity;
			string pref = boardQuantity == 1 ? "board" : "boards";
			return $"{boardQuantity} {pref}";

		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
