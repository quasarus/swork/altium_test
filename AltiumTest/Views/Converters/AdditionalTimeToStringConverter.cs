﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace AltiumTest.Views.Converters
{
    public class AdditionalTimeToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
	        uint? addDays = (uint?)value;

	        if (addDays == null)
		        return string.Empty;

	        return $"+{(uint)addDays} days";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
