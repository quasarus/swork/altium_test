﻿namespace AltiumTest.Validators
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;
	using AltiumTest.Model.Preferences;
	using AltiumTest.ViewModels;


	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
	public class QuantityCodeValidationAttribute : ValidationAttribute
	{
		protected override ValidationResult IsValid(object value, ValidationContext validationContext)
		{
			if (validationContext.Items.FirstOrDefault().Value is not IValidatableBoardsQuantity validatedObj)
			{
				throw new ArgumentNullException(nameof(validatedObj));
			}

			string input = (string) value;

			if (string.IsNullOrEmpty(input))
			{
				return new ValidationResult("Enter quantity value.");
			}


			if (uint.TryParse(input, out uint val))
			{
				if (val == 0)
				{
					return new ValidationResult("Quantity can't be equal 0.");
				}

				validatedObj.Quantity = val;
				return ValidationResult.Success;
			}

			return new ValidationResult("Enter valid post code value.");

		}
	}
}
