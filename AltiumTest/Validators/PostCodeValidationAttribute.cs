﻿namespace AltiumTest.Validators
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;
	using AltiumTest.Model.Preferences;
	using AltiumTest.ViewModels;


	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
	public class PostCodeValidationAttribute : ValidationAttribute
	{
		protected override ValidationResult IsValid(object value, ValidationContext validationContext)
		{
			if (validationContext.Items.FirstOrDefault().Value is not IValidatableZipCode validatedObj)
			{
				throw new ArgumentNullException(nameof(validatedObj));
			}

			string input = (string) value;

			if (string.IsNullOrEmpty(input))
			{
				return new ValidationResult("Enter post code value.");
			}

			if (!input.All(char.IsDigit))
			{
				return new ValidationResult("Entered post code value must be a digits.");
			}

			if (input.Length != 5)
			{
				return new ValidationResult("Enter 5-digit post code value.");
			}

			if (uint.TryParse(input, out uint val))
			{
				if (val == 0)
				{
					return new ValidationResult("Enter post code can't be equal 0.");
				}

				validatedObj.PostcodeValue = val;
				return ValidationResult.Success;
			}

			return new ValidationResult("Enter valid post code value.");

		}
	}
}
