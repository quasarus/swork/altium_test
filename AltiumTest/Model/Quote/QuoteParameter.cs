﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Mvvm;

namespace AltiumTest.Model.Quote
{
    public abstract class QuoteParameter : BindableBase
    {
        private string _name;
        public string Name
        {
            get => _name;
            set
            {
                SetProperty(ref _name, value);
            }
        }
        private string _stringValue;
        public string StringValue
        {
            get => _stringValue;
            set
            {
                SetProperty(ref _stringValue, value);
            }
        }

        private string _timeImpact;
        public string TimeImpact
        {
            get => _timeImpact;
            set
            {
                SetProperty(ref _timeImpact, value);
            }
        }

        private string _costImpactstr;
        public string CostImpact
        {
            get => _costImpactstr;
            set
            {
                SetProperty(ref _costImpactstr, value);
            }
        }

        private string _group;
        public string Group
        {
	        get => _group;
	        set
	        {
		        SetProperty(ref _group, value);
	        }
        }

		public uint ImpactDaysValue { get; set; }
		public decimal CostImpactValue { get; set; }
        public QuoteParameter(string name, string value, uint timeImpactDays, decimal costImpact)
        {
            Name = name;
            StringValue = value;
            ImpactDaysValue = timeImpactDays;
            CostImpactValue = costImpact;
            TimeImpact = timeImpactDays == 0 ? "-" : $"{timeImpactDays} day";
            CostImpact = $"${costImpact.ToString("0.00")}";
        }
    }

    public class QuoteParameterFabrication : QuoteParameter
    {

	    public QuoteParameterFabrication(string name, string value, uint timeImpactDays, decimal costImpact) : base(name, value, timeImpactDays, costImpact)
	    {
            Group = "Fabrication";

        }
    }

    public class QuoteParameterAssembly : QuoteParameter
    {

	    public QuoteParameterAssembly(string name, string value, uint timeImpactDays, decimal costImpact) : base(name, value, timeImpactDays, costImpact)
	    {
		    Group = "Assembly";

	    }
    }

    public class QuoteParameterComponents : QuoteParameter
    {

	    public QuoteParameterComponents(string name, string value, uint timeImpactDays, decimal costImpact) : base(name, value, timeImpactDays, costImpact)
	    {
		    Group = "Components";

	    }
    }

}
