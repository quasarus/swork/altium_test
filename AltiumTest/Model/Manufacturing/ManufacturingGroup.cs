﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Mvvm;

namespace AltiumTest.Model.Manufacturing
{
	public class ManufacturingGroup : BindableBase
	{
		private string _name;
		public string Name
		{
			get => _name;
			set => SetProperty(ref _name, value);
		}

		private decimal _cost;
		public decimal Cost
		{
			get => _cost;
			set => SetProperty(ref _cost, value);
		}

		public int DayImpact { get; set; }


		public ManufacturingGroup(string name, decimal cost, int dayImpact)
		{
			Name = name;
			Cost = cost;
			DayImpact = dayImpact;

		}
	}
}
