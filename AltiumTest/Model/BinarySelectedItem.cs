﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Mvvm;

namespace AltiumTest.Model
{
	public class BinarySelectedItem : BindableBase
	{
		public string Name { get; set; }



		private bool _isSelected;
		public bool IsSelected
		{
			get { return _isSelected; }
			set
			{
				SetProperty(ref _isSelected, value);
			}
		}

		public BinarySelectedItem(string name)
		{
			Name = name;
		}
	}
}
