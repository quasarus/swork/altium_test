﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using AltiumTest.Model.Preferences;
using Prism.Mvvm;

namespace AltiumTest.Model
{
    public class PreferencesGroup : BindableBase
    {
		private string _name;
		public string Name
		{
			get => _name;
			set => SetProperty(ref _name, value);
		}

		private string _description;
		public string Description
		{
			get => _description;
			set => SetProperty(ref _description, value);
		}

		private IEnumerable<Preference> _preferences;
		public IEnumerable<Preference> Preferences
		{
			get => _preferences;
			set => SetProperty(ref _preferences, value);
		}

		public void SetDefaultPreferences()
		{
			foreach (var preference in Preferences)
			{
				preference.SetDefault();
			}
		}

		public PreferencesGroup(string name, string description, IEnumerable<Preference> preferences)
		{
			Name = name;
			Description = description;
			Preferences = preferences;
		}
	}
}
