﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Mvvm;

namespace AltiumTest.Model.Fabrication
{

	public abstract class FabricationItem : BindableBase
	{
		private int _timeImpactScore;
		public int TimeImpactScore
		{
			get => _timeImpactScore;
			set => SetProperty(ref _timeImpactScore, value);
		}

		private int _costImpactScore;
		public int CostImpactScore
		{
			get => _costImpactScore;
			set => SetProperty(ref _costImpactScore, value);
		}

		private string _strValue;
		public string StrValue
		{
			get => _strValue;
			set => SetProperty(ref _strValue, value);
		}

		private string _name;
		public virtual string Name
		{
			get => throw new NotImplementedException();
			set => SetProperty(ref _name, value);
		}



	}


	public abstract class FabricationItem<T> : FabricationItem
	{


		public T Value { get; set; }


		public FabricationItem(T newValue)
		{
			Value = newValue;

			StrValue = newValue.ToString();
		}

		protected void CheckFabricationItemConditions(IEnumerable<FabricationItemCondition<T>> conditions, T newValue)
		{
			if (conditions == null)
			{
				throw new ArgumentNullException(nameof(conditions));
			}

			var respectedConfitions = conditions.Where(t => t.VerifyRespected(newValue)).ToList();

			if (respectedConfitions.Count() != 1)
			{
				throw new InvalidOperationException("Wrong check of fabrication conditions result.");
			}

			TimeImpactScore = respectedConfitions.First().TimeScore;
			CostImpactScore = respectedConfitions.First().CostScore;
		}


		protected virtual IEnumerable<FabricationItemCondition<T>> GetConditions()
		{
			throw new NotImplementedException();
		}

		private IEnumerable<FabricationItemCondition<T>> _conditions;
		public IEnumerable<FabricationItemCondition<T>> Conditions
		{
			get { return _conditions; }
			set => SetProperty(ref _conditions, value);
		}
	}



	public sealed class FabricationItemLayers : FabricationItem<int>
	{
		public FabricationItemLayers(int newValue) : base(newValue)
		{
			Conditions = GetConditions();
			CheckFabricationItemConditions(Conditions, newValue);
		}


		protected override IEnumerable<FabricationItemCondition<int>> GetConditions()
		{
			List<FabricationItemCondition<int>> desc = new()
			{
				new FabricationItemCondition<int>(false, "", 0, 0, (layerNum) => layerNum <= 0),
				new FabricationItemCondition<int>(true, "up to 8", 1, 1, (layerNum) => layerNum > 1 && layerNum < 8),
				new FabricationItemCondition<int>(true, "up to 18", 2, 2, (layerNum) => layerNum >= 8 && layerNum < 18),
				new FabricationItemCondition<int>(true, "18-64", 3, 3, (layerNum) => layerNum >= 18 && layerNum <= 64),
				new FabricationItemCondition<int>(true, ">64", 3, 3, (layerNum) => layerNum > 64),
				
			};

			return desc;
		}

		public override string Name { get => "Layers"; set => base.Name = value; }
	}

	public sealed class FabricationItemBoardThickness : FabricationItem<double>
	{
		public FabricationItemBoardThickness(double newValue) : base(newValue)
		{
			Conditions = GetConditions();
			CheckFabricationItemConditions(Conditions, newValue);
			StrValue = $"{newValue.ToString("0.00")}mm";
		}


		protected override IEnumerable<FabricationItemCondition<double>> GetConditions()
		{
			List<FabricationItemCondition<double>> desc = new()
			{
				new FabricationItemCondition<double>(true, "up to 1.57", 3, 3, (thikness) => thikness > 0 && thikness <= 1.57),
				new FabricationItemCondition<double>(true, "up to 2.54", 4, 4, (thikness) => thikness > 1.57 && thikness < 2.54),

			};

			return desc;
		}

		public override string Name { get => "Board Thickness"; set => base.Name = value; }
	}
}
