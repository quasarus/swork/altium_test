﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Mvvm;

namespace AltiumTest.Model.Fabrication
{
	public class FabricationItemCondition<T> : BindableBase
	{
		private string _description;
		public string Description
		{
			get => _description;
			set => SetProperty(ref _description, value);
		}

		private int _costScore;
		public int CostScore
		{
			get => _costScore;
			set => SetProperty(ref _costScore, value);
		}

		private int _timeScore;
		public int TimeScore
		{
			get => _timeScore;
			set => SetProperty(ref _timeScore, value);
		}

		private bool _isRespected;
		public bool IsRespected
		{
			get => _isRespected;
			set => SetProperty(ref _isRespected, value);
		}

		private bool _isVisible;
		public bool IsVisible
		{
			get => _isVisible;
			set => SetProperty(ref _isVisible, value);
		}

		private Predicate<T> _predicate;
		public bool VerifyRespected(T newValue)
		{
			IsRespected = _predicate.Invoke(newValue);
			return IsRespected;
		}

		public FabricationItemCondition(bool isVisible, string description, int costScore, int timeScore,  Predicate<T> predicate)
		{
			IsVisible = isVisible;
			_predicate = predicate;
			CostScore = costScore;
			TimeScore = timeScore;
			Description = description;
		}
	}
}
