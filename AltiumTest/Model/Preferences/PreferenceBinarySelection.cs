﻿using System.Collections.Generic;
using System.Linq;

namespace AltiumTest.Model.Preferences
{
	public class PreferenceBinarySelection : Preference
	{
		private BinarySelectedItem _selectedItem;
		public BinarySelectedItem SelectedItem
		{
			get => _selectedItem;
			set => 
				SetProperty(ref _selectedItem, value);
		}


		private List<BinarySelectedItem> _items;
		public List<BinarySelectedItem> Items
		{
			get => _items;
			set => SetProperty(ref _items, value);
		}

		public PreferenceBinarySelection()
		{
			Items = new List<BinarySelectedItem>();
		}
	}

	public class PreferenceBinaryLeadFree : PreferenceBinarySelection
	{
		public PreferenceBinaryLeadFree()
		{
			Items.Add(new BinarySelectedItem("Yes"));
			Items.Add(new BinarySelectedItem("No"));
			Name = "Lead Free";
		}

		public override void SetDefault()
		{
			Items.ElementAt(0).IsSelected = true;
		}
	}

	public class PreferenceIPCClass : PreferenceBinarySelection
	{
		public PreferenceIPCClass()
		{
			Items.Add(new BinarySelectedItem("Class 2"));
			Items.Add(new BinarySelectedItem("Class 3"));
			Name = "IPC Class";
		}
		public override void SetDefault()
		{
			Items.ElementAt(0).IsSelected = true;
		}
	}

	public class PreferenceITAR : PreferenceBinarySelection
	{
		public PreferenceITAR()
		{
			Items.Add(new BinarySelectedItem("Yes"));
			Items.Add(new BinarySelectedItem("No"));
			Name = "ITAR";
		}

		public override void SetDefault()
		{
			Items.ElementAt(1).IsSelected = true;
		}
	}

	public class PreferenceFluxType : PreferenceBinarySelection
	{
		public PreferenceFluxType()
		{
			Items.Add(new BinarySelectedItem("Clean"));
			Items.Add(new BinarySelectedItem("No Clean"));
			Name = "Flux Type";
		}
		public override void SetDefault()
		{
			Items.ElementAt(1).IsSelected = true;
		}
	}

	public class PreferenceControlledImpedance : PreferenceBinarySelection
	{
		public PreferenceControlledImpedance()
		{
			Items.Add(new BinarySelectedItem("None"));
			Items.Add(new BinarySelectedItem("See Notes"));
			Name = "Controlled Impedance";
		}

		public override void SetDefault()
		{
			Items.ElementAt(0).IsSelected = true;
		}
	}

	public class PreferenceTentingForVias : PreferenceBinarySelection
	{
		public PreferenceTentingForVias()
		{
			Items.Add(new BinarySelectedItem("Both Sides"));
			Items.Add(new BinarySelectedItem("Top Side"));
			Items.Add(new BinarySelectedItem("Bottom Side"));
			Items.Add(new BinarySelectedItem("None"));
			Name = "Tenting For Vias";
		}

		public override void SetDefault()
		{
			Items.ElementAt(3).IsSelected = true;
		}
	}

	public class PreferenceStackup : PreferenceBinarySelection
	{
		public PreferenceStackup()
		{
			Items.Add(new BinarySelectedItem("Standart"));
			Items.Add(new BinarySelectedItem("See Notes"));
			Name = "Stackup";
		}
		public override void SetDefault()
		{
			Items.ElementAt(0).IsSelected = true;
		}
	}
}