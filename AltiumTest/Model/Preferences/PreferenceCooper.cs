﻿using System.Linq;

namespace AltiumTest.Model.Preferences
{
	public class CooperWeight
	{
		public decimal Weight { get; set; }
		public CooperWeight(decimal weight)
		{
			Weight = weight;
		}
	}

	public class PreferenceCooperInner : PreferenceComboBox<CooperWeight>
	{
		public PreferenceCooperInner()
		{
			Items.Add(new ComboBoxItem<CooperWeight>("1.0 oz", new CooperWeight(1m)));
			Items.Add(new ComboBoxItem<CooperWeight>("2.0 oz", new CooperWeight(2m)));
			Name = "Cooper Weight on Inner Layers";
		}

		public override void SetDefault()
		{
			SelectedPayload = Items.ElementAt(0).Payload;

		}
	}

	public class PreferenceCooperOuter : PreferenceComboBox<CooperWeight>
	{
		public PreferenceCooperOuter()
		{
			Items.Add(new ComboBoxItem<CooperWeight>("1.0 oz", new CooperWeight(1m)));
			Items.Add(new ComboBoxItem<CooperWeight>("2.0 oz", new CooperWeight(2m)));
			Name = "Cooper Weight on Outer Layers";
		}

		public override void SetDefault()
		{
			SelectedPayload = Items.ElementAt(0).Payload;

		}
	}
}