﻿using System.Linq;
using System.Windows.Media;

namespace AltiumTest.Model.Preferences
{
	public class PreferenceSolderMask : PreferenceComboBox<SolidColorBrush>
	{
		public PreferenceSolderMask()
		{
			Items.Add(new ComboBoxItem<SolidColorBrush>("Green", new SolidColorBrush(Color.FromRgb(0,255,0))));
			Items.Add(new ComboBoxItem<SolidColorBrush>("Red", new SolidColorBrush(Color.FromRgb(255, 0, 0))));
			Name = "Solder Mask Color";
		}

		public override void SetDefault()
		{
			SelectedPayload = Items.ElementAt(0).Payload;
		}
	}
}