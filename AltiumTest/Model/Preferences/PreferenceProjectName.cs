﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace AltiumTest.Model.Preferences
{

	public class PreferenceTextBox : Preference
	{
		public int DataLen { get; set; }


        private string _input;
		public virtual string Input
		{
			get => _input;
			set => SetProperty(ref _input, value);
		}


	}

	public class PreferenceTextBoxValidatable : PreferenceTextBox, INotifyDataErrorInfo
	{

        #region Validation
        protected ConcurrentDictionary<string, List<ValidationResult>> validationErrors = new();
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        private bool _isInputValid;

        public bool IsInputValid
        {
            get { return _isInputValid; }
            set { SetProperty(ref _isInputValid, value); }
        }
        protected void NotifyErrorsChanged([CallerMemberName] string propertyName = "")
        {
            if (ErrorsChanged != null)
                ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyName));
        }


        public bool HasErrors
        {
            get
            {
                IsInputValid = validationErrors.Count == 0;

                return !IsInputValid;
            }
        }

        public IEnumerable GetErrors(string propertyName)
        {
            if (propertyName == null)
            {
                return null;
            }

            validationErrors.TryGetValue(propertyName, out var propertyErrors);
            return propertyErrors;
        }

        protected void ValidateInput(object? ob, string propertyName)
        {
            List<ValidationResult> existingErrors;
            validationErrors.TryRemove(propertyName, out existingErrors);

            var validationResults = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(this) { MemberName = propertyName };
            context.Items.Add("", this);


            if (!Validator.TryValidateProperty(ob, context, validationResults) && validationResults.Count > 0)
            {
                _ = validationErrors.AddOrUpdate(propertyName, new List<ValidationResult>()
                    {
                        new ValidationResult(validationResults[0].ErrorMessage)
                    },
                    (key, existingVal) =>
                    {
                        return new List<ValidationResult>()
                        {
                            new ValidationResult(validationResults[0].ErrorMessage)
                        };
                    });

                
            }
            NotifyErrorsChanged(propertyName);
        }

        #endregion
    }


    public class PreferenceProjectName: PreferenceTextBox
	{
		public PreferenceProjectName()
		{
			Name = "Project Name";
			DataLen = 50;


        }
		public override void SetDefault()
		{
			Input = "Default Name";

		}
    }
}
