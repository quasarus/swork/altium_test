﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;
using AltiumTest.Validators;

namespace AltiumTest.Model.Preferences
{
	public interface IValidatableBoardsQuantity
	{
		public uint Quantity { get; set; }
	}


	public class PreferenceBoardsQuantity : PreferenceTextBoxValidatable, IValidatableBoardsQuantity, IAdditionalValues
	{
		private const decimal _oneBoardCost = 26.9m;

	    public PreferenceBoardsQuantity()
	    {
			Name = "Boards Quantity";
			DataLen = 10;
			ValidateInput(Input, nameof(Input));
		}

	    private string _input;
	    [QuantityCodeValidation]
	    public override string Input
	    {
		    get { return _input; }
		    set
		    {
			    SetProperty(ref _input, value);
			    ValidateInput(Input, nameof(Input));
		    }
	    }

		public uint Quantity { get; set; }
        public decimal AdditionalCost { get => Quantity * _oneBoardCost; set => throw new NotImplementedException(); }
        public uint AdditionalTime { get => 0; set => throw new NotImplementedException(); }

        public override void SetDefault()
		{
			Input = "300";
		}
	}
}
