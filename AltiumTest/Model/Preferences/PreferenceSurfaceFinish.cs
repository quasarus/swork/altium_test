﻿using System.Linq;

namespace AltiumTest.Model.Preferences
{
	public class PreferenceSurfaceFinish : PreferenceComboBox<SurfaceFinish>
	{
		public PreferenceSurfaceFinish()
		{
			Items.Add(new ComboBoxItem<SurfaceFinish>("ENEPIG", new SurfaceFinish(30, 2, "ENEPIG")));
			Items.Add(new ComboBoxItem<SurfaceFinish>("XXXXXX", new SurfaceFinish(75, 1, "XXXXXX")));
			Name = "Surface Finish";
		}

		public override void SetDefault()
		{
			SelectedPayload = Items.ElementAt(0).Payload;
		}
	}
}