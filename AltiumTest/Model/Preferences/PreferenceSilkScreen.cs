﻿using System.Linq;
using System.Windows.Media;

namespace AltiumTest.Model.Preferences
{
	public class PreferenceSilkScreen : PreferenceComboBox<SolidColorBrush>
	{
		public PreferenceSilkScreen()
		{
			Items.Add(new ComboBoxItem<SolidColorBrush>("Black", new SolidColorBrush(Color.FromRgb(0, 0, 0))));
			Items.Add(new ComboBoxItem<SolidColorBrush>("White", new SolidColorBrush(Color.FromRgb(255, 255, 255))));
			Name = "Silk Screen Color";
		}

		public override void SetDefault()
		{
			SelectedPayload = Items.ElementAt(1).Payload;

		}
	}
}