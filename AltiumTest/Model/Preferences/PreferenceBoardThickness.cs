﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AltiumTest.Model.Preferences
{
    public class PreferenceBoardThickness : PreferenceTextBox
    {
	    public PreferenceBoardThickness()
	    {
			Name = "Board Thickness";
			DataLen = 10;
	    }

	    public override void SetDefault()
	    {
		    Input = "1,57mm";
	    }
	}
}
