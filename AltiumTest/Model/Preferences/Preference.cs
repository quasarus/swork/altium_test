﻿using Prism.Mvvm;

namespace AltiumTest.Model.Preferences
{
	public abstract class Preference : BindableBase
	{
		private string _name;
		public string Name
		{
			get => _name;
			set => SetProperty(ref _name, value);
		}

		public virtual void SetDefault()
		{

		}
	}
}
