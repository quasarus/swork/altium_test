﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Mvvm;

namespace AltiumTest.Model.Preferences
{
    public interface IAdditionalValues
    {
        decimal AdditionalCost { get; set; }
        uint AdditionalTime { get; set; }
    }


    public class BoardMaterial : BindableBase, IAdditionalValues
    {

        public decimal AdditionalCost { get; set; }
        public uint AdditionalTime { get; set; }

        public BoardMaterial(decimal additionalCost, uint additionalTime)
        {
            AdditionalCost = additionalCost;
            AdditionalTime = additionalTime;
        }
    }

    public class SurfaceFinish : BoardMaterial
    {
	    public string Value { get; set; }

	    public SurfaceFinish(decimal additionalCost, uint additionalTime, string value) : base(additionalCost, additionalTime)
	    {
            Value = value;
	    }
    }


    public class ComboBoxItem<T>
    {
        public string Description { get; set; }
        public T Payload { get; set; }

        public ComboBoxItem(string description, T payload)
        {
            Description = description;
            Payload = payload;
        }
    }


    public class PreferenceComboBox<T> : Preference
    {
        private T _selPayload;
        public T SelectedPayload
        {
            get => _selPayload;
            set
            {
                SetProperty(ref _selPayload, value);
                OnItemSelected(value);
            }

        }

        private decimal _additionalCost;
        public decimal AdditionalCost
        {
            get => _additionalCost;
            set
            {
                SetProperty(ref _additionalCost, value);
            }
        }

        private uint _additionalTime;
        public uint AdditionalTime
        {
            get => _additionalTime;
            set
            {
                SetProperty(ref _additionalTime, value);
            }
        }

        private void OnItemSelected(T value)
        {
            if (value is IAdditionalValues val)
            {
                AdditionalCost = val.AdditionalCost;
                AdditionalTime = val.AdditionalTime;
            }
        }

        private List<ComboBoxItem<T>> _items;
        public List<ComboBoxItem<T>> Items
        {
            get => _items;
            set => SetProperty(ref _items, value);
        }

        public PreferenceComboBox()
        {
            Items = new List<ComboBoxItem<T>>();

        }
    }

    public class PreferenceMaterial : PreferenceComboBox<BoardMaterial>
    {
        public PreferenceMaterial()
        {
            Items.Add(new ComboBoxItem<BoardMaterial>("Arlon", new BoardMaterial(100, 2)));
            Items.Add(new ComboBoxItem<BoardMaterial>("Textolite", new BoardMaterial(10, 1)));
            Name = "Material";
        }

        public override void SetDefault()
        {
            SelectedPayload = Items.ElementAt(0).Payload;
        }

    }
}
