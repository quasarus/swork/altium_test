﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AltiumTest.Model.Preferences
{
	public class PreferenceNotes : PreferenceTextBox
	{
		public PreferenceNotes()
		{
			Name = "Notes";
			DataLen = 110;
		}

		public override void SetDefault()
		{
			Input = "Default notes";

		}
	}
}
