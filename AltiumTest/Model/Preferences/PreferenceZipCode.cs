﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using AltiumTest.Validators;

namespace AltiumTest.Model.Preferences
{
	public interface IValidatableZipCode
	{
		public uint PostcodeValue { get; set; }
	}


    public class PreferenceZipCode : PreferenceTextBoxValidatable, IValidatableZipCode
    {
        public PreferenceZipCode()
        {
            Name = "Zipcode";
            DataLen = 5;
            ValidateInput(Input, nameof(Input));
        }


        private string _input;
        [PostCodeValidation]
        public override string Input
        {
            get { return _input; }
            set
            {
                SetProperty(ref _input, value);
                ValidateInput(Input, nameof(Input));
            }
        }

        private uint _postcodeValue;
        public uint PostcodeValue
        {
            get { return _postcodeValue; }
            set { SetProperty(ref _postcodeValue, value); }
        }


        private string _projectName;

        public string ProjectName
        {
            get { return _projectName; }
            set
            {
                SetProperty(ref _projectName, value);
            }
        }


        public override void SetDefault()
        {
	        Input = "12345";
        }

    }
}
