﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using AltiumTest.Exstensions;
using AltiumTest.Model;
using AltiumTest.Model.Fabrication;
using AltiumTest.Model.Manufacturing;
using AltiumTest.Model.Preferences;
using AltiumTest.Model.Quote;
using Prism.Commands;
using Prism.Mvvm;

namespace AltiumTest.ViewModels
{
	public enum MainWindowState
	{
		Preferences,
		QuotesOrders
	}



	public class MainWindowViewModel : BindableBase
	{
		private string _title = "Altium test Application";
		public string Title
		{
			get => _title;
			set => SetProperty(ref _title, value);
		}

		private MainWindowState _mainWindowState;
		public MainWindowState MainWindowState
		{
			get => _mainWindowState;
			set
			{
				SetProperty(ref _mainWindowState, value);
			}

		}

		private bool _isPreferencesExpanded;
		public bool IsPreferencesExpanded
		{
			get => _isPreferencesExpanded;
			set
			{
				SetProperty(ref _isPreferencesExpanded, value);
				if (value)
				{
					MainWindowState = MainWindowState.Preferences;
					IsPreferencesExpanded = false;
				}
			}

		}

		private bool _canPlaceAnOrder;
		public bool CanPlaceOrder
		{
			get => _canPlaceAnOrder;
			set
			{
				SetProperty(ref _canPlaceAnOrder, value);
			}
		}

		private bool _isPreferencesValid;
		public bool IsPreferencesValid
		{
			get => _isPreferencesValid;
			set
			{
				SetProperty(ref _isPreferencesValid, value);
			}
		}

		public DelegateCommand SetDefaultCommand { get; set; }
		public DelegateCommand SaveAndContinueCommand { get; set; }
		private ObservableCollection<PreferencesGroup> _preferenceGroups;
		public ObservableCollection<PreferencesGroup> PreferenceGroups
		{
			get => _preferenceGroups;
			set => SetProperty(ref _preferenceGroups, value);
		}

		private ObservableCollection<QuoteParameter> _quoteParameters;
		public ObservableCollection<QuoteParameter> QuoteParameters
		{
			get => _quoteParameters;
			set => SetProperty(ref _quoteParameters, value);
		}
		private ObservableCollection<ManufacturingGroup> _manufacturing;
		public ObservableCollection<ManufacturingGroup> Manufacturing
		{
			get => _manufacturing;
			set => SetProperty(ref _manufacturing, value);
		}

		private ObservableCollection<FabricationItem> _fabrication;
		public ObservableCollection<FabricationItem> Fabrication
		{
			get => _fabrication;
			set => SetProperty(ref _fabrication, value);
		}

		public MainWindowViewModel()
		{


			List<Preference> prefs1 = new List<Preference>()
			{
				new PreferenceProjectName(),
				new PreferenceZipCode(),
				new PreferenceBoardsQuantity()
			};
			List<Preference> prefs2 = new List<Preference>()
			{
				new PreferenceBoardThickness(),
				new PreferenceMaterial(),
				new PreferenceSurfaceFinish(),
				new PreferenceSolderMask()
			};
			List<Preference> prefs3 = new List<Preference>()
			{
				new PreferenceBinaryLeadFree(),
				new PreferenceIPCClass(),
				new PreferenceITAR(),
				new PreferenceFluxType(),
				new PreferenceSilkScreen(),
				new PreferenceCooperInner(),
				new PreferenceCooperInner(),
				new PreferenceControlledImpedance(),
				new PreferenceTentingForVias(),
				new PreferenceStackup(),
				new PreferenceNotes()

			};

			PreferenceGroups = new ObservableCollection<PreferencesGroup>();
			PreferenceGroups.Add(new PreferencesGroup("Project Basics", "Preferences on this page supersede design file notes", prefs1));
			PreferenceGroups.Add(new PreferencesGroup("Important Board Preferences", "Preferences on this page supersede design file notes", prefs2));
			PreferenceGroups.Add(new PreferencesGroup("Special Board Preferences", "Preferences on this page supersede design file notes", prefs3));

			TrackPreferencesErrors(PreferenceGroups);

			SetDefaultCommand = new DelegateCommand(() =>
			{
				PreferenceGroups.SetDefaultPreferences();
			});
			SaveAndContinueCommand = new DelegateCommand(() =>
			{
				
				QuoteParameters = FillQuoteParameters();
				Manufacturing = new ObservableCollection<ManufacturingGroup>(FillManufacturing(QuoteParameters));
				Fabrication = new ObservableCollection<FabricationItem>(FillFabrication(QuoteParameters));
				MainWindowState = MainWindowState.QuotesOrders;
				CanPlaceOrder = true;
			});

		}

		private IEnumerable<FabricationItem> FillFabrication(IEnumerable<QuoteParameter> quoteParameters)
		{
			List<FabricationItem> fabricationItems = new();
			fabricationItems.Add(new FabricationItemLayers(10));
			fabricationItems.Add(new FabricationItemBoardThickness(1.57));
			
			//TODO add some

			return fabricationItems;
		}


		private IEnumerable<ManufacturingGroup> FillManufacturing(IEnumerable<QuoteParameter> quoteParameters)
		{
			List<ManufacturingGroup> manuf = new();
			var grouped = quoteParameters.GroupBy(f => f.Group);

			foreach (var group in grouped)
			{
				if (!@group.Any())
					continue;

				manuf.Add(new ManufacturingGroup(group.Key, group.Sum(d => d.CostImpactValue),
					(int)group.Sum(g => g.ImpactDaysValue)));
			}

			return manuf;
		}

		private IEnumerable<INotifyDataErrorInfo> _validatablePreferences;
		private void TrackPreferencesErrors(ObservableCollection<PreferencesGroup> preferenceGroups)
		{
			_validatablePreferences = preferenceGroups.SelectMany(t => t.Preferences).OfType<INotifyDataErrorInfo>();
			foreach (var pref in _validatablePreferences)
			{
				pref.ErrorsChanged += Pref_ErrorsChanged;
			}
			Pref_ErrorsChanged(null, null);
		}

		private void Pref_ErrorsChanged(object sender, DataErrorsChangedEventArgs e)
		{
			if (_validatablePreferences == null)
				return;
			IsPreferencesValid = _validatablePreferences.All(p => !p.HasErrors);
		}

		private ObservableCollection<QuoteParameter> FillQuoteParameters()
		{
			ObservableCollection<QuoteParameter> quoteParameters = new();
			quoteParameters.AddBoardsQuantity(PreferenceGroups);
			quoteParameters.AddSurfaceFinish(PreferenceGroups);
			quoteParameters.AddAdditional(PreferenceGroups);
			return quoteParameters;
		}
	}
}
